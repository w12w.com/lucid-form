export type Size = 'tiny' | 'small' | 'medium' | 'large'
