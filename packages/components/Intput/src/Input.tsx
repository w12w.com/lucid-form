import { resolveBem } from "../../../utils/resolver";
import { PropType } from "vue";
import {Size} from "../../../utils/types";
export const inputProps = {
  bordered: {
    type: Boolean as PropType<boolean | undefined>,
    default: undefined
  },
  type: {
    type: String as PropType<'text' | 'textarea' | 'password'>,
    default: 'text'
  },
  placeholder: [Array, String] as PropType<string | [string, string]>,
  defaultValue: {
    type: [String, Array] as PropType<null | string | [string, string]>,
    default: null
  },
  value: [String, Array] as PropType<null | string | [string, string]>,
  disabled: {
    type: Boolean as PropType<boolean | undefined>,
    default: undefined
  },
  size: String as PropType<Size>,
  rows: {
    type: [Number, String] as PropType<number | string>,
    default: 3
  },
  round: Boolean,
  minlength: [String, Number] as PropType<number | string>,
  maxlength: [String, Number] as PropType<number | string>,
  clearable: Boolean,
  autosize: {
    type: [Boolean, Object] as PropType<
      boolean | { minRows?: number, maxRows?: number }
    >,
    default: false
  },
  pair: Boolean,
  separator: String,
  readonly: {
    type: [String, Boolean],
    default: false
  },
  passivelyActivated: Boolean,
  showPasswordOn: String as PropType<'mousedown' | 'click'>,
  stateful: {
    type: Boolean,
    default: true
  },
  autofocus: Boolean,
  // inputProps: Object as PropType<TextareaHTMLAttributes | InputHTMLAttributes>,
  resizable: {
    type: Boolean,
    default: true
  },
  showCount: Boolean,
  loading: {
    type: Boolean,
    default: undefined
  },
}
export default {
  props: inputProps,
  model:{
    prop: 'value',
    event: 'updateValue'
  },
  created(){
  },
  methods:{
    input(e){
      this.$emit('updateValue', e.target.value)
    },
  },
  render(){
    const input = resolveBem('input')
    const inputAttrs = {
      class: input.e('inner')
    }
    return <div class={input.b()}>
      <input value={this.value}  vOn:input={this.input}  placeholder="Name" {...{ attrs: inputAttrs }}/>
    </div>
  }
}
