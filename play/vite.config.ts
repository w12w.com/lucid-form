import { createVuePlugin } from 'vite-plugin-vue2'
import vueJsx from '@vitejs/plugin-vue2-jsx'

// https://vitejs.dev/config/
export default {
  // server: {
  //   watch: {
  //     // 监控/src/components/下面的组件改变 就会触发hmr
  //     ignored: ['!../packages/components/**']
  //   }
  // },

  plugins: [createVuePlugin(),

    vueJsx({
      // options are passed on to @vue/babel-preset-jsx
    })],
}
