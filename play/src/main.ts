import Vue from 'vue'
import TestContainer from './TestContainer.vue'
import "../../packages/theme-chalk/src/index.scss";

new Vue({
  render: h => h(TestContainer)
}).$mount('#app')
